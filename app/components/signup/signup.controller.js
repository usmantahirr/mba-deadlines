/**
 * Created by ghazala on 08/08/2015.
 */

(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .controller('Signup', SignupController);
    SignupController.$inject = ['$state', 'userService', '$cookies', '$rootScope'];

    function SignupController($state, userService, $cookies, $rootScope)  {
        var vm = this;
        vm.state = $state;
        vm.user = {};
        vm.valid = false;

        init();
        // Exposed Variables
        // Exposed Methods
        vm.addUser = addUser;

        // Function definitions
        function addUser () {
            $rootScope.isLoading = true;
            // Email
            vm.valid = vm.user.email;
            // PW
            vm.valid = vm.user.password;
            // FName
            vm.valid = vm.user.first_name;
            // LName
            vm.valid = vm.user.last_name;

            if (vm.valid) {
                userService.data.save(vm.user, function (user) {
                    $cookies.put('auth', user._id);
                    $rootScope.isLoading = false;
                    $state.go('step2');
                }, function (obj) {
                    ohSnap(obj.data.error, 'Red');
                    $rootScope.isLoading = false;
                });
            } else {
                ohSnap('Incomplete Data', 'Red');
                $rootScope.isLoading = false;
            }
        }

        function init() {
            $('.salutation.ui.dropdown').dropdown({
                onChange: function (val) {
                    vm.user.salutation = val;
                }
            });
            $('.country.ui.dropdown').dropdown({
                onChange: function (val) {
                    vm.user.country = val;
                }
            });
            $('.signup.form').form({
                on: 'blur',
                fields: {
                    email: {
                        identifier  : 'email',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'email is required'
                            },
                            {
                                type   : 'email',
                                prompt : 'Please enter a valid e-mail'
                            }
                        ]
                    },
                    password: {
                        identifier : 'password',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Please enter a password'
                            },
                            {
                                type   : 'minLength[6]',
                                prompt : 'Your password must be at least 6 characters'
                            }
                        ]
                    },
                    name: {
                        identifier : 'name',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Name is required field'
                            }
                        ]
                    },
                    salutation: {
                        identifier : 'salutation',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Salutation is required field'
                            }
                        ]
                    }
                }
            });
        }
    }
}());
