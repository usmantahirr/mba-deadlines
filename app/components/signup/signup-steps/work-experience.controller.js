(function () {
    'use strict';

    angular
        .module('mba-deadlines')
        .controller('SUWorkExperience', WorkExperienceController);

    WorkExperienceController.$inject = ['$state', '$rootScope', '$cookies', '$scope', 'userService', 'MBA_INDUSTRIES'];
    function WorkExperienceController($state, $rootScope, $cookies, $scope, userService, MBA_INDUSTRIES) {
        $rootScope.isLoading = false;
        var vm = this;

        vm.workExperience = {};
        vm.industries = MBA_INDUSTRIES;

        vm.isValid = false;
        vm.addWorkExperience = addWorkExperience;
        vm.actionHandler = actionHandler;

        $scope.$on('$viewContentLoaded', function(){
            init();
        });

        function actionHandler (mode) {
            if (mode === 'back') {
                addWorkExperience('su-student-profile');
            } else {
                addWorkExperience('su-education');
            }
        }

        function addWorkExperience(state) {
            $rootScope.isLoading = true;
            if (!vm.workExperience.duration && !vm.workExperience.pre_mba && !vm.workExperience.post_mba) {
                vm.isValid = false;
                $rootScope.isLoading = false;
            } else if (vm.workExperience.duration || vm.workExperience.pre_mba || vm.workExperience.post_mba) {
                vm.isValid = true;
                $rootScope.isLoading = false;
            }

            if (vm.isValid) {
                userService.data.update({'id': $cookies.get('auth')}, {"work_experience": vm.workExperience}, function (argument) {
                    ohSnap(argument.Success, 'Green');
                    debugger;
                    $rootScope.isLoading = false;
                    $state.go(state);
                })
            } else {
                ohSnap('Data Missing', 'Red');
            }
        }

        function init () {
            debugger;
            $('.pre.dropdown').dropdown({
                onChange: function (val) {
                    vm.workExperience.pre_mba = val;
                }
            });

            $('.post.dropdown').dropdown({
                onChange: function (val) {
                    vm.workExperience.post_mba = val;
                }
            });
        }
    }
}());
