(function () {
    'use strict';

    angular
        .module('mba-deadlines')
        .controller('SUEducation', EducationController);

    EducationController.$inject = ['$state', '$rootScope', '$cookies', '$scope', 'userService', 'EDUCATION_AREAS'];
    function EducationController($state, $rootScope, $cookies, $scope, userService, EDUCATION_AREAS) {
        $rootScope.isLoading = false;
        var vm = this;

        vm.education = {};
        vm.educationAreas = EDUCATION_AREAS;

        vm.isValid = false;
        vm.addEducation = addEducation;
        vm.actionHandler = actionHandler;

        $scope.$on('$viewContentLoaded', function(){
            init();
        });

        function actionHandler (mode) {
            if (mode === 'back') {
                addEducation('ug-work-experience');
            } else {
                addEducation('dashboard');
            }
        }

        function addEducation(state) {
            $rootScope.isLoading = true;
            if (!vm.education.totalCgpa && !vm.education.obtCgpa && !vm.education.major && !vm.year) {
                vm.isValid = false;
                $rootScope.isLoading = false;
            } else if (vm.education.totalCgpa || vm.education.obtCgpa || vm.education.major || vm.year) {
                vm.isValid = true;
                $rootScope.isLoading = false;
            }

            if (vm.isValid) {
                userService.data.update({'id': $cookies.get('auth')}, {"education": vm.education}, function (argument) {
                    ohSnap(argument.Success, 'Green');
                    $rootScope.isLoading = false;
                    $state.go(state);
                })
            } else {
                ohSnap('Data Missing', 'Red');
            }
        }

        function init () {
            $('.ug-major.dropdown').dropdown({
                onChange: function (val) {
                    vm.education.major = val;
                }
            });
        }
    }
}());
