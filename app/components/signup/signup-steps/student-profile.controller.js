(function() {
    "use strict";

    angular
        .module('mba-deadlines')
        .controller('SUStudentProfile',studentProfileController);

    studentProfileController.$inject = ['$state', '$rootScope', '$cookies', 'userService'];

    function studentProfileController($state, $rootScope, $cookies, userService) {
        $rootScope.isLoading = false;
        var vm = this;

        vm.studentProfile = {};

        vm.isValid = false;
        vm.addStudentProfile = addStudentProfile;


        function addStudentProfile () {
            $rootScope.isLoading = true;
            if (!vm.studentProfile.gre && !vm.studentProfile.gat) {
                vm.isValid = false;
                $rootScope.isLoading = false;
            } else if (vm.studentProfile.gre || vm.studentProfile.gat) {
                vm.isValid = true;
                $rootScope.isLoading = false;
            }

            if (vm.isValid) {
                userService.data.update({'id': $cookies.get('auth')}, { "student_profile": vm.studentProfile}, function (argument) {
                    ohSnap(argument.Success, 'Green');
                    $rootScope.isLoading = false;
                    $state.go('su-work-experience');
                })
            } else {
                ohSnap('Data Missing', 'Red');
            }
        }
    }

}());
