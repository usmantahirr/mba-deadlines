
(function () {
    'use strict';

    angular
        .module('mba-deadlines')
        .controller('Profile', ProfileController);

    ProfileController.$inject = ['$scope','$cookies', 'userService', 'EDUCATION_AREAS', 'MBA_INDUSTRIES'];
    function ProfileController($scope, $cookies, userService, EDUCATION_AREAS, MBA_INDUSTRIES) {
        var vm = this;

        // Exposed Variables
        vm.user = {};
        vm.educationAreas = EDUCATION_AREAS;
        vm.industries = MBA_INDUSTRIES;
        vm.isLoading = false;
        vm.p1 = '';
        $scope.p2 = '';
        vm.pwValidity = false;

        // Exposed Functions
        vm.updateData = function () {
            vm.isLoading = true;
            userService.data.update({'id': $cookies.get('auth')}, vm.user).$promise.then(function () {
                vm.isLoading = false;
                ohSnap('Record Updated', 'Green');
            });
        };

        $scope.$watch('p2', function(nv, ov) {
            if (nv) {
                if (vm.p1 === nv) {
                    vm.pwValidity = true;
                } else {
                    vm.pwValidity = false;
                }
            } else {
                vm.pwValidity = false;
            }
        });

        vm.changePassword = function () {
            vm.isLoading = true;
            userService.data.update({'id': $cookies.get('auth')}, {'password': vm.p1}).$promise.then(function () {
                vm.p1 = '';
                $scope.p2 = '';
                vm.isLoading = false;
                ohSnap('Password Changed', 'Green');
            });
        }

        init();
        function init() {
            vm.isLoading = true;
            userService.data.get({id: $cookies.get('auth')}).$promise.then(function (data) {
                vm.user = data;

                if (!vm.user.student_profile) {
                    vm.user.student_profile = {
                        gre: {},
                        gat: {}
                    };
                }
                if (!vm.user.work_experience) {
                    vm.user.work_experience = {};
                }
                if (!vm.user.education) {
                    vm.user.education = {};
                }

                if (vm.user.dob) {
                    vm.user.dob = new Date(vm.user.dob);
                }
                if (vm.user.student_profile.gat.dateTaken) {
                    vm.user.student_profile.gat.dateTaken = new Date(vm.user.student_profile.gat.dateTaken);
                }
                if (vm.user.student_profile.gre.dateTaken) {
                    vm.user.student_profile.gre.dateTaken = new Date(vm.user.student_profile.gre.dateTaken);
                }
                delete vm.user._id;
                // Salutation Dropdown
                $('.salutation.ui.dropdown').dropdown({
                    onChange: function(text, value) {
                        vm.user.salutation = value;
                    }
                }).dropdown('set selected', vm.user.salutation);


                $('.country.ui.dropdown').dropdown({
                    onChange: function(text, value) {
                        vm.user.country = value;
                    }
                }).dropdown('set selected', vm.user.country);

                $('.pre.ui.dropdown').dropdown({
                    onChange: function(text, value) {
                        vm.user.work_experience.pre_mba = value;
                    }
                }).dropdown('set selected', vm.user.work_experience.pre_mba);

                $('.post.ui.dropdown').dropdown({
                    onChange: function(text, value) {
                        vm.user.work_experience.post_mba = value;
                    }
                }).dropdown('set selected', vm.user.work_experience.post_mba);

                $('.ug-major.ui.dropdown').dropdown({
                    onChange: function(text, value) {
                        vm.user.education.major = value;
                    }
                }).dropdown('set selected', vm.user.education.major);
                vm.isLoading = false;
            });
        }
    }
} ());
