(function() {
    "use strict";

    angular
        .module('mba-deadlines')
        .config(userProfileRouter);

    userProfileRouter.$inject = ['$stateProvider'];
    function userProfileRouter($stateProvider) {
        $stateProvider.state('userProfile', {
            parent: 'index',
            url: '/profile',
            views: {
                'content@': {
                    templateUrl: 'components/user-profile/user-profile.tpl.html',
                    controller: 'Profile',
                    controllerAs: 'profile'
                }
            }
        });
    }

}());

