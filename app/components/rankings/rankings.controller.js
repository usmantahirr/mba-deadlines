(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .controller('Rankings', AdminRankings);
    AdminRankings.$inject = ['schoolsService', '$rootScope'];
    function AdminRankings(schoolsService, $rootScope) {

        var vm = this;

        vm.searchQuery = '';
        vm.isLoading = false;
        vm.data = [];

        // Sort Details
        vm.sortType = 'school'; // set the default sort type
        vm.sortReverse = false;  // set the default sort order

        init();

        vm.getRankStatus = getRankStatus;

        function getRankStatus (status) {
            var ret = '';
            if (status === 'up') {
                ret = 'up green';
            } else if (status === 'down') {
                ret = 'down red';
            } else {
                ret = 'minus yellow';
            }
            return ret;
        }

        function init() {
            vm.isLoading = true;
            schoolsService.rest.query(function(schools) {
                vm.data = schools;
                vm.isLoading = false;
            });
        }
    }
}());
