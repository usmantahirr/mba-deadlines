/**
 * Created by ghazala on 08/08/2015.
 */

(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .controller('Login', LoginController);
    LoginController.$inject = ['$state', '$cookies', 'userService', '$rootScope'];

    function LoginController($state, $cookies, userService, $rootScope) {
        var vm = this;

        // Exposed Variables
        vm.email = '';
        vm.password = '';
        vm.rememberFlag = false;

        // Exposed Methods
        vm.auth = login;

        function login() {

            userService.rest.post({email: vm.email, password: vm.password}).$promise.then(function (data) {
                vm.email = data.email;
            }).catch(function (error) {

            } );

            userService.rest.post({email: vm.email, password: vm.password}, function (obj) {
                if (obj._id) {
                    $cookies.put('auth', obj._id);
                    $rootScope.portalUser = obj;
                    $state.go('dashboard');
                } else {
                    ohSnap('Email/Password not valid', 'red');
                }
            });
        }

    }
}());
