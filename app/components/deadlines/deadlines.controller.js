(function () {
    'use strict';

    angular
        .module('mba-deadlines')
        .controller('Deadlines', DeadlinesController);

    DeadlinesController.$inject = ['schoolsService', '$rootScope', '$cookies'];
    function DeadlinesController(schoolsService, $rootScope, $cookies) {

        // Local Variables
        var vm = this;

        // Exposed Variables
        vm.searchQuery = '';
        vm.data = [];
        vm.selectedSchool = {};
        vm.selectedRound = '';

        // Exposed Functions
        vm.addHover = addHover;
        vm.addSchool = addSchool;
        vm.selectRound = selectRound;
        vm.isSelected = isSelected;
        vm.isLoading = false;
        $rootScope.tz = $('#timezonePicker').val();

        // Sort Details
        vm.sortType = 'school'; // set the default sort type
        vm.sortReverse = false;  // set the default sort order

        init();

        vm.checkExistance = function (schoolId) {
            for (var index in vm.bookmarkedSchools) {
                if (vm.bookmarkedSchools[index].school_id === schoolId) {
                    return true;
                }
            }
            return false;
        };

        // Function Definitions
        function addHover(deadline) {
            // Shows/hides the add button on hover
            return deadline.showAdd = !deadline.showAdd;
        }

        function bookMarkRound() {
            var temp = vm.selectedSchool;

            if (vm.selectedRound === 1) {
                temp.selectedRound = temp.roundOne;
                temp.selectedRound.round = 1;
            } else if (vm.selectedRound === 2) {
                temp.selectedRound = temp.roundTwo;
                temp.selectedRound.round = 2;
            } else if (vm.selectedRound === 3) {
                temp.selectedRound = temp.roundThree;
                temp.selectedRound.round = 3;
            } else if (vm.selectedRound === 4) {
                temp.selectedRound = temp.roundFour;
                temp.selectedRound.round = 4;
            } else {
                ohSnap('No Match Found', 'warning');
                console.log('No Match Found');
            }

            if (temp) {
                temp.user_id = $cookies.get('auth');
            }

            schoolsService.bookmarks.save(temp, function(obj) {
                vm.bookmarkedSchools.push(obj);
                ohSnap(temp.school + ' bookmarked', 'green');
            }, function(error) {
                ohSnap('Fail: ' + error.toString(), 'red');
            });
        }

        function addSchool(school) {
            vm.selectedSchool = school;
            $('.deadline-uni-details-modal.ui.modal').modal({
                onHide: function () {
                    vm.selectedRound = '';
                },
                 onApprove: function() {
                    bookMarkRound();
                }
            })
            .modal('show');
        }

        function selectRound(round) {
            vm.selectedRound = round;
        }

        function isSelected(round) {
            return vm.selectedRound === round;
        }

        function init() {
            vm.isLoading = true;
            schoolsService.bookmarks.get({ user_id: $cookies.get('auth') }, function (object) {
                vm.bookmarkedSchools = object;

                schoolsService.rest.query(function (schools) {
                    vm.data = schools;
                    vm.isLoading = false;
                }, function (error) {
                    ohSnap('FAIL: Schools not loaded, please reload page', 'red');
                    console.log(error);
                });

            }, function (error) {
                ohSnap('Fail: Bookmarks not loaded, please reload page', 'red');
                console.log(error);
            });

        }

    }
}());
