(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .controller('Dashboard', DashboardController);
    DashboardController.$inject = ['$scope', 'schoolsService', '$rootScope', '$cookies', '$http', 'BASE_URL'];

    function DashboardController($scope, schoolsService, $rootScope, $cookies, $http, BASE_URL) {

        // Local Variables
        var vm = this;
        var addDropDown = '';

        // Sort Details
        vm.sortType = 'school'; // set the default sort type
        vm.sortReverse = false;  // set the default sort order

        $rootScope.tz = $('#timezonePicker').val();

        vm.timezoneChanged = function () {
        };
        function init () {
            $('#timezone').timezones();
        }
        init();

        // Exposed Variables
        /**
         * Array which stores only bookmarked school objects
         * @type {Array}
         */
        vm.bookmarkedSchools = [];
        /**
         * Array which holds all School Objects
         * @type {Array}
         */
        vm.allSchools = [];
        /**
         * Object which holds class profile for specific school. Its updated every time you ask for class profile
         * as it only stores content of single object
         * @type {Object}
         */
        vm.classProfile = [];
        /**
         * Object which holds only selected school content during bookmark process.
         * This object is reset to empty when modal is closed.
         * @type {Object}
         */
        vm.selectedSchool = {};
        /**
         * Stores selected round number during bookmark process
         * @type {string}
         */
        vm.selectedRound = '';

        // Exposed Functions
        vm.isSchoolListEmpty = isSchoolListEmpty;
        vm.addSchool = addSchool;
        vm.selectRound = selectRound;
        vm.isSelected = isSelected;
        vm.bookMarkRound = bookMarkRound;
        vm.isDropdownSelected = isDropdownSelected;
        vm.bookmarked = isBookmarked;
        vm.getClassProfile = getClassProfile;
        vm.deleteBookmark = removeBookmark;
        vm.edit = editHandler;
        vm.isLoading = false;

        initController();
        // Function Definitions
        // Helper Functions

        /**
         * When used click on add, it adds bookmark to bookmark list
         */
        function bookMarkRound() {
            var temp = vm.selectedSchool;

            if (vm.selectedRound === 1) {
                temp.selectedRound = temp.roundOne;
                temp.selectedRound.round = 1;
            } else if (vm.selectedRound === 2) {
                temp.selectedRound = temp.roundTwo;
                temp.selectedRound.round = 2;
            } else if (vm.selectedRound === 3) {
                temp.selectedRound = temp.roundThree;
                temp.selectedRound.round = 3;
            } else if (vm.selectedRound === 4) {
                temp.selectedRound = temp.roundFour;
                temp.selectedRound.round = 4;
            } else {
                ohSnap('No Match Found', 'warning');
                console.log('No Match Found');
            }

            if (temp) {
                temp.user_id = $cookies.get('auth');
            }

            schoolsService.bookmarks.save(temp, function(obj) {
                vm.bookmarkedSchools.push(obj);
                ohSnap(temp.school + ' bookmarked', 'green');
            }, function(error) {
                ohSnap('Fail: ' + error.toString(), 'red');
            });

        }

        /**
         * Function called when ever any bookmark deleted
         * @param  {int} bokmark_id
         */
        function removeBookmark(id) {
            var user = $cookies.get('auth');
            $('.delete-modal.modal').modal({
                onApprove: function() {
                    $http.delete(BASE_URL + '/bookmarks/' + id, {
                        params: {
                            user_id: user
                        }
                    }).success(function(data, status) {
                        vm.bookmarkedSchools = vm.bookmarkedSchools.filter(function(el) {
                            return el._id !== id
                        });
                        ohSnap('Record Removed', 'green');
                        console.log(data, status);
                    }).error(function(error, status) {
                        ohSnap('Error Code: ' + status, 'red');
                    });
                }
            }).modal('show');
        }

        function editHandler(id) {
            var user = $cookies.get('auth');
            var editor1, editor2, editor3;
            $('.edit-modal.modal').modal({
                onShow: function() {
                    $('.essay.ui.accordion').accordion();
                    editor1 = new Simditor({
                        textarea: $('#essay1_editor'),
                        upload: false,
                        tabIndent: true,
                        toolbar: [
                            'bold',
                            'italic',
                            'underline',
                            'ul',
                            'blockquote',
                            'hr'
                        ],
                        toolbarFloat: false,
                        toolbarFloatOffset: 0,
                        toolbarHidden: false,
                        pasteImage: false
                    });
                    editor2 = new Simditor({
                        textarea: $('#essay2_editor')
                    });
                    editor3 = new Simditor({
                        textarea: $('#essay3_editor')
                    });
                },
                onApprove: function() {
                }
            }).modal('show');
        }
        /**
         * Check to tell is school list empty
         * @returns {boolean}
         */
        function isSchoolListEmpty() {
            return !vm.bookmarkedSchools.length > 0;
        }

        /**
         * Add school to bookmarks, it initiates @bookmarkSchool function when finish preprocessing.
         */
        function addSchool() {

            $('.dash-add-school.coupled.modal').modal({
                allowMultiple: false
            });

            // First Modal
            $('.dash-search-school.first.modal').modal({
                onShow: function() {
                    addDropDown = $('.school-list.dropdown').dropdown({
                        onChange: function(school) {
                            vm.selectedSchool = school;
                        }
                    });
                }
            }).modal('show');

            // Second Modal
            $('.dash-select-session.second.modal').modal({
                onShow: function() {
                    $scope.$apply();
                },
                onHide: function() {
                    vm.selectedRound = '';
                    vm.selectedSchool = {};
                    addDropDown.dropdown('clear');
                },
                onApprove: function() {
                    vm.bookMarkRound();
                }
            }).modal('attach events', '.dash-search-school.first.modal .proceed');
        }

        /**
         * Select round from modal. It Assigns the object corresponding to round number passed
         * @param round
         */
        function selectRound(round) {
            vm.selectedRound = round;
        }

        /**
         * check if round your are passing down is selected or not
         * @param round
         * @returns {boolean}
         */
        function isSelected(round) {
            return vm.selectedRound === round;
        }

        /**
         * check if item in bookmark modal dropdown is selected or not.
         * @returns {{}|*}
         */
        function isDropdownSelected() {
            return vm.selectedSchool;
        }

        /**
         * Show class profile on a new Modal
         * @param profile
         */
        function getClassProfile(profile) {
            vm.classProfile = [];
            for (var key in profile) {
                if (profile[key].value) {
                    vm.classProfile = profile;
                }
            }
            $('.class-profile.modal').modal('show');
        }

        /**
         * Check if the school object you are passing is already bookmarked or not
         * @param school
         * @returns {boolean}
         */
        function isBookmarked(school) {
            if (vm.bookmarkedSchools.length > 0) {
                for (var i = 0; i < vm.bookmarkedSchools.length; i++) {
                    if (vm.bookmarkedSchools[i].school_id === school._id) {
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * Controller Initializer
         */
        function initController() {

            vm.isLoading = true;

            // Variable Initializations
            schoolsService.rest.query(function(schools) {
                vm.allSchools = schools;
            });
            schoolsService.bookmarks.get({
                user_id: $cookies.get('auth')
            }, function(object) {
                vm.bookmarkedSchools = object;
                vm.isLoading = false;
            });
        }
        $scope.$on('LastElem', function(event){
            $('.action-group .trophy').popup({
                title: 'Class Profile',
                content: 'Details about last year\'s class'
            });
            $('.action-group .write').popup({
                title: 'Application',
                content: 'Edit required Essays & Minor Questions'
            });
            $('.action-group .newspaper').popup({
                title: 'Apply Onsite',
                content: 'Go to on-site Application details'
            });
            $('.action-group .world').popup({
                content: 'Web'
            });
            $('.action-group .remove').popup({
                content: 'Remove School'
            });
        });
    }
}());
