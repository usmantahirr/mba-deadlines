(function() {
    "use strict";

    angular
        .module('mba-deadlines')
        .config(dashboardRouter)
        .directive('repeatEnd', function() {
            return function(scope, element, attrs) {
                if (scope.$last){
                    scope.$emit('LastElem');
                }
            };
        });

    dashboardRouter.$inject = ['$stateProvider'];
    function dashboardRouter($stateProvider) {
        $stateProvider.state('dashboard', {
            parent: 'index',
            data: {'requireAuth': true},
            url: '/dashboard',
            views: {
                'content@': {
                    templateUrl: 'components/dashboard/dashboard.tpl.html',
                    controller: 'Dashboard',
                    controllerAs: 'dashboard'
                }
            }
        });

        $stateProvider.state('user-essay', {
            parent: 'dashboard',
            data: {'requireAuth': true},
            url: '/?b_id?s_id',
            views: {
                'content@': {
                    templateUrl: 'components/dashboard/essays.tpl.html',
                    controller: 'Essays',
                    controllerAs: 'essays'
                }
            }
        });
    }

}());
