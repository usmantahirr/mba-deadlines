(function () {
    angular
        .module('mba-deadlines')
        .controller('Essays', essaysController);

    essaysController.$inject = ['$scope', '$cookies', '$stateParams', 'essayService'];
    function essaysController($scope, $cookies, $stateParams, essayService) {
        var vm = this;

        vm.data = [];
        vm.isLoading = true;
        vm.crudInProgress = false;

        vm.add = addEssay;
        vm.update = updateEssay;

        initController();

        vm.trimHtml = function (content) {
            return $(content).text();
        };

        vm.countWords = function (content) {
            var s = vm.trimHtml(content);
            s = s.replace(/(^\s*)|(\s*$)/gi,'');
            s = s.replace(/[ ]{2,}/gi,' ');
            s = s.replace(/\n /,'\n');
            return s.split(' ').length;
        };

        function addEssay(essay) {
            vm.crudInProgress = true;
            essay.school_id = $stateParams.s_id;
            essay.user_id = $cookies.get('auth');
            delete essay.question;
            delete essay.limit;
            delete essay.required;
            essayService.rest.save(essay, function () {
                vm.crudInProgress = false;
                initController();
            });

        }

        function updateEssay(essay) {
            vm.crudInProgress = true;
            delete essay.question;
            delete essay.limit;
            delete essay.required;
            essayService.rest.update({'essay_id': essay._id}, essay, function (obj) {
                vm.crudInProgress = false;
                initController();
            });
        }

        function initController () {
            vm.isLoading = true;
            essayService.rest.query({'school_id': $stateParams.s_id, 'user_id': $cookies.get('auth')}, function(object) {
                vm.data = object;
                vm.isLoading = false;
            });
        }

    }
}());
