(function() {
    "use strict";

    angular
        .module('mba-deadlines')
        .config(AdminDashboardRouter);

    AdminDashboardRouter.$inject = ['$stateProvider'];
    function AdminDashboardRouter($stateProvider) {
        $stateProvider.state('admindashboard', {
            parent: 'index',
            data: {requireAdminAuth: true, noNavbar: true},
            url: '/admin',
            redirectTo: 'adminStats',
            views: {
                'content@': {
                    templateUrl: 'components/admin-dashboard/admin-dashboard.tpl.html',
                    controller: 'AdminDashboard',
                    controllerAs: 'admindashboard'
                }
            }
        });
    }

}());
