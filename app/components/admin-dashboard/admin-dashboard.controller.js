(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .controller('AdminDashboard', AdminDashboardController);
    AdminDashboardController.$inject = ['adminService'];
    function AdminDashboardController(adminService) {
        this.adminLogout = function () {
            adminService.logout();
        }
    }
}());
