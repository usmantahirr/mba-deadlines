(function () {
    "use strict";

    angular
        .module('mba-deadlines')
        .config(dashboardRouter);

    dashboardRouter.$inject = ['$stateProvider'];
    function dashboardRouter($stateProvider) {
        $stateProvider.state('admin-login', {
            parent: 'index',
            url: '/admin-login',
            data: {noNavbar: true},
            views: {
                'content@': {
                    templateUrl: 'components/admin-dashboard/admin-login/admin-login.tpl.html',
                    controller: 'AdminLogin',
                    controllerAs: 'adminLogin'
                }
            }
        });
    }

}());
