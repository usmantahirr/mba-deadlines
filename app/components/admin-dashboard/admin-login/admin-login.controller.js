(function () {
    "use strict";

    angular
        .module('mba-deadlines')
        .controller('AdminLogin', AdminLogin);

    AdminLogin.$inject = ['adminService', '$cookies', '$state'];
    function AdminLogin(adminService, $cookies, $state) {
        var vm = this;

        vm.email = '';
        vm.password = '';

        vm.authenticate = function () {
            adminService.rest.save({email: vm.email, password: vm.password}, function (admin) {
                if (admin) {
                    $cookies.put('admin', admin._id);
                    $state.go('admindashboard');
                } else {
                    ohSnap('Email/Password not valid', 'red');
                }
            });
        };
    }
}());
