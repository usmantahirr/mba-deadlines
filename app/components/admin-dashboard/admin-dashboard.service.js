(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .factory('adminServices', adminServices);

    adminServices.$inject = ['BASE_URL','$resource', '$http'];

    function adminServices (BASE_URL, $resource, $http) {

    	var schoolResource = $resource(BASE_URL + '/schools', {});
    	var httpSVC = $http(BASE_URL + '/schools/:id', {});

    	return {
    		rest: schoolResource,
    		http: httpSVC
    	}
    }

}());