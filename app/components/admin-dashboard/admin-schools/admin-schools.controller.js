(function () {
    'use strict';

    angular
        .module('mba-deadlines')
        .controller('AdminSchools', AdminSchoolsController);
    AdminSchoolsController.$inject = ['schoolsService', '$http', 'BASE_URL', '$rootScope', '$scope', 'adminSchoolsSvc', 'flowFactory'];

    function AdminSchoolsController(schoolsService, $http, BASE_URL, $rootScope, $scope, adminSchoolsSvc, flowFactory) {

        // Local Variables
        var vm = this,
            classProfile = [
                { 'name': 'Applicants'},
                { 'name': 'Enrollment'},
                { 'name': 'Admitted'},
                { 'name': 'Average Age'},
                { 'name': 'Countries Represented'},
                { 'name': 'International Students'},
                { 'name': 'Women'},
                { 'name': 'GMAT Range'},
                { 'name': 'Median GMAT'},
                { 'name': 'Avg GPA'}
            ];

        // Exposed Variables
        vm.searchQuery = '';
        vm.allSchools = [];
        vm.isUS = false;

        vm.tempEssayFlag = false;
        vm.tempEssay = {};

        vm.selectedSchool = {};
        vm.mode = '';

        $scope.csv = {
            content: null,
            header: true,
            headerVisible: true,
            separator: ',',
            separatorVisible: true,
            result: null,
            encoding: 'ISO-8859-1',
            encodingVisible: true,
        };

        vm.showCSV = function () {
            $('.csv.ui.modal').modal('show');
        };

        // Exposed Functions
        vm.delete = deleteSchool;
        vm.showSchool = showSchool;
        vm.addSchool = addSchool;
        vm.updateSchool = updateSchool;

        vm.activeTab = 'school'; // school | cp | iu

        vm.addEssay = addTempEssay;
        vm.acceptEssay = pushTempEssay;
        vm.discardEssay = discardTempEssay;
        vm.flowFile = flowFactory.create();

        function initController () {
            $rootScope.isLoading = true;
            schoolsService.rest.query(function (schools) {
                vm.allSchools = schools.map(function (data, index, array) {
                    if (!data.class_profile) {
                        data.class_profile = classProfile;
                    }
                    return data;
                });
                $rootScope.isLoading = false;
            });
        }

        function discardTempEssay () {
            vm.tempEssay = {};
            vm.tempEssayFlag = false;
        }
        function pushTempEssay () {
            if (!vm.selectedSchool.essays) {
                vm.selectedSchool.essays = [];
            }
            vm.selectedSchool.essays.push(vm.tempEssay);
            vm.tempEssayFlag = false;
        }
        function addTempEssay () {
            vm.tempEssayFlag = true;
        }

        function updateState(code, state) {
            vm.selectedSchool.state = code;
        }

        function updateCountry(code, country) {
            if (country) {
                var cont = country;
                vm.isUS = code === 'us';

                var ci = cont.indexOf('i>');
                cont = cont.slice(ci + 2);

                vm.selectedSchool.country = {
                    code: code,
                    name: cont
                };
            }
        }

        function showSchool (mode, school) {

            $('.label-help').popup({
                title: 'Rank Status',
                content: 'Comparison about rank going up, down or is constant as last years ratting.'
            });

            $('.add-essay').popup({
                title: 'Add Essay',
                content: 'Add new Essay topic?'
            });


            if (mode === 'edit') {
                vm.selectedSchool = school;
                vm.mode = 'Edit';

                //debugger;
                vm.imageExistance = vm.selectedSchool.icon.length > 50;

                $('.country.ui.dropdown').dropdown({
                    onChange: function (code, country) {
                        updateCountry(code, country);
                    }
                }).dropdown('set selected', vm.selectedSchool.country.code);

                $('.state.ui.dropdown').dropdown({
                    onChange: function (code, state) {
                        updateState(code, state);
                    }
                }).dropdown('set selected', vm.selectedSchool.state);


            } else if (mode === 'add') {
                vm.flowFile.files = [];
                vm.selectedSchool = {
                    class_profile: classProfile
                };
                vm.mode = 'Add';
                $('.country.ui.dropdown').dropdown({
                    onChange: function (code, country) {
                        updateCountry(code, country);
                    }
                });

                $('.state.ui.dropdown').dropdown({
                    onChange: function (code, state) {
                        updateState(code, state);
                    }
                });
            }

            $('.add-modal.ui.modal').modal({
                closable: false,
                onShow: function () {
                    // convertAllDates(vm.selectedSchool, false);
                },

                onHide: function () {
                    clearAll();
                },
                onDeny: function () {
                    return true;
                },
                onApprove: function () {
                    if (vm.mode === 'Edit') {
                        updateSchool();
                    }
                    if (vm.mode === "Add") {
                        addSchool();
                    }
                    return true;
                }
            }).modal('show');
        }

        $scope.$watch('vm.selectedSchool.roundOne', function () {
        });

        // POST Request for adding new school
        function addSchool() {
            vm.selectedSchool.icon = $('[flow-img]').attr('src');
            adminSchoolsSvc.rest.save(vm.selectedSchool, function () {
                refreshPage();
                ohSnap('Record Added', 'Green');
            });
        }

        // PUT Requiest for updating already existed school
        function updateSchool() {
            vm.selectedSchool.icon = $('[flow-img]').attr('src');
            adminSchoolsSvc.rest.update({'school_id': vm.selectedSchool._id},vm.selectedSchool, function (obj) {
                refreshPage();
                ohSnap(obj.Success, 'Green');
            });
        }

        /**
         * Function called when ever any bookmark deleted
         * @param  {int} bokmark_id
         */
        function deleteSchool(id) {
            $('.delete-modal.modal').modal({
                onApprove: function () {
                    $http.delete(BASE_URL + '/schools/' + id, {}).success(function (data, status) {
                        // vm.bookmarkedSchools = vm.bookmarkedSchools.filter(function(el) {
                        //     return el._id !== id
                        // })
                        refreshPage();
                        ohSnap('Record Removed', 'green');
                        console.log(data, status);
                    }).error(function (error, status) {
                        ohSnap('Error Code: ' + status, 'red');
                    });

                }
            }).modal('show');
        }

        /**
         * Convert BSON ISO Date to UTC String
         * @param date BSON ISO
         * @param isDate is it a date or string. if date, convert to string, else convert to UTC
         * if false, it will return date, if true, it will return string
         * @returns {Date} UTC
         */
        function convertDate (date, isDate) {
            if (!isDate) {
                return new Date(date);
            }
            return date.toString();
        }

        function convertAllDates(selectedSchool, isDate) {
            // Round One
            if (selectedSchool.roundOne) {
                if (selectedSchool.roundOne.deadline) {
                    selectedSchool.roundOne.deadline = convertDate(selectedSchool.roundOne.deadline, isDate);
                }
                if (selectedSchool.roundOne.notification) {
                    selectedSchool.roundOne.notification = convertDate(selectedSchool.roundOne.notification, isDate);
                }
            }

            // Round Two
            if (selectedSchool.roundTwo) {
                if (selectedSchool.roundTwo.deadline) {
                    selectedSchool.roundTwo.deadline = convertDate(selectedSchool.roundTwo.deadline, isDate);
                }
                if (selectedSchool.roundTwo.notification) {
                    selectedSchool.roundTwo.notification = convertDate(selectedSchool.roundTwo.notification, isDate);
                }
            }

            // Round Three
            if (selectedSchool.roundThree) {
                if (selectedSchool.roundThree.deadline) {
                    selectedSchool.roundThree.deadline = convertDate(selectedSchool.roundThree.deadline, isDate);
                }
                if (selectedSchool.roundThree.notification) {
                    selectedSchool.roundThree.notification = convertDate(selectedSchool.roundThree.notification, isDate);
                }
            }

            // Round Four
            if (selectedSchool.roundFour) {
                if (selectedSchool.roundFour.deadline) {
                    selectedSchool.roundFour.deadline = convertDate(selectedSchool.roundFour.deadline, isDate);
                }
                if (selectedSchool.roundFour.notification) {
                    selectedSchool.roundFour.notification = convertDate(selectedSchool.roundFour.notification, isDate);
                }
            }
        }

        function clearAll() {
            console.log('hidden');
            vm.selectedSchool = {};
            vm.mode = '';
            vm.tempEssayFlag = false;
            vm.tempEssay = {};
            vm.activeTab = 'school';
            vm.flowFile.files = [];
            vm.imageExistance = false;
            $('.ui.dropdown').dropdown('clear');
            $('.country.ui.dropdown').dropdown('clear');
            $('.state.ui.dropdown').dropdown('clear');
        }

        function refreshPage () {
            initController();
        }

        initController();


    }
}());
