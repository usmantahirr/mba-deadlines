(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .factory('adminSchoolsSvc', adminSchoolsSvc);

    adminSchoolsSvc.$inject = ['BASE_URL', '$resource'];

    function adminSchoolsSvc (BASE_URL, $resource) {
        var rankingsResource = $resource(BASE_URL + '/schools/:school_id', {}, {
            update: {method: 'PUT'}
        });

        return {
            rest: rankingsResource
        }
    }

}());