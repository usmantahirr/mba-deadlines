(function() {
    "use strict";

    angular
        .module('mba-deadlines')
        .config(AdminSchoolsRouter);

    AdminSchoolsRouter.$inject = ['$stateProvider'];
    function AdminSchoolsRouter($stateProvider) {
        $stateProvider.state('adminSchools', {
            parent: 'admindashboard',
            url: '/schools',
            views: {
                'adminContent': {
                    templateUrl: 'components/admin-dashboard/admin-schools/admin-schools.tpl.html',
                    controller: 'AdminSchools',
                    controllerAs: 'adminSchools'
                }
            }
        });
    }

}());
