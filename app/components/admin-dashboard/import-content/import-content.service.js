(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .factory('importContentSvc', importContentSvc);

    importContentSvc.$inject = ['BASE_URL', '$resource'];

    function importContentSvc (BASE_URL, $resource) {
        return $resource(BASE_URL + '/import', {}, {
            update: {method: 'PUT'}
        });
    }

}());