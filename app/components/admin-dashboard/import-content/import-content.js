(function() {
    "use strict";

    angular
        .module('mba-deadlines')
        .config(importContent);

    importContent.$inject = ['$stateProvider'];
    function importContent($stateProvider) {
        $stateProvider.state('import-content', {
            parent: 'admindashboard',
            url: '/import-content',
            views: {
                'adminContent': {
                    templateUrl: 'components/admin-dashboard/import-content/import-content.tpl.html',
                    controller: 'ImportContent',
                    controllerAs: 'importContent'
                }
            }
        });
    }

}());
