(function () {
    'use strict';

    angular
        .module('mba-deadlines')
        .factory('adminDeadlinesSvc', adminDeadlinesService);

    adminDeadlinesService.$inject = ['BASE_URL', '$resource'];

    function adminDeadlinesService(BASE_URL, $resource) {
        var deadlinesResource = $resource(BASE_URL + '');

        return {
            rest: deadlinesResource
        }
    }

}());
