(function() {
    "use strict";

    angular
        .module('mba-deadlines')
        .config(AdminDeadlinesRouter);

    AdminDeadlinesRouter.$inject = ['$stateProvider'];
    function AdminDeadlinesRouter($stateProvider) {
        $stateProvider.state('adminDeadlines', {
            parent: 'admindashboard',
            url: '/deadlines',
            views: {
                'adminContent': {
                    templateUrl: 'components/admin-dashboard/admin-deadlines/admin-deadlines.tpl.html',
                    controller: 'AdminDeadlines',
                    controllerAs: 'aDeadlines'
                }
            }
        });
    }

}());
