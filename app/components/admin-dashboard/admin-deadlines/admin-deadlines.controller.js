(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .controller('AdminDeadlines', AdminDeadlines);
    AdminDeadlines.$inject = ['schoolsService', '$rootScope'];
    function AdminDeadlines (schoolsService, $rootScope) {

        // Local Variables
        var vm = this;

        // Exposed Variables
        vm.searchQuery = '';
        vm.data = [];
        vm.selectedSchool = {};
        vm.selectedRoud = '';

        // Exposed Functions
        vm.addSchool = addSchool;
        vm.selectRound = selectRound;
        vm.isSelected = isSelected;
        $rootScope.tz = $('#timezonePicker').val();

        init();

        function addSchool (school) {
            vm.selectedSchool = school;
            $('.uni-details-modal.ui.modal')
                .modal({
                    onHide: function () {
                        vm.selectedRoud = '';
                    }
                })
                .modal('show');
        }

        function selectRound ( round ) {
            vm.selectedRoud = round;
        }

        function isSelected ( round ) {
            return vm.selectedRoud === round;
        }

        function init() {
            $rootScope.isLoading = true;
            schoolsService.rest.query(function(schools) {
                //processDates(schools);
                vm.data = schools;
                $rootScope.isLoading = false;
            });
        }

        function processDates(schools) {
            for (var i = 0; i < schools.length; i++) {
                schools[i].roundOne = new Date(schools[i].roundOne.deadline);
                schools[i].roundTwo = new Date(schools[i].roundTwo.deadline);
                schools[i].roundThree = new Date(schools[i].roundThree.deadline);
                schools[i].roundFour = new Date(schools[i].roundFour.deadline);
            }
        }

    }
}());
