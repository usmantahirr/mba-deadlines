(function() {
    "use strict";

    angular
        .module('mba-deadlines')
        .config(AdminRankingsRouter);

    AdminRankingsRouter.$inject = ['$stateProvider'];
    function AdminRankingsRouter($stateProvider) {
        $stateProvider.state('adminRankings', {
            parent: 'admindashboard',
            url: '/rankings',
            views: {
                'adminContent': {
                    templateUrl: 'components/admin-dashboard/admin-rankings/admin-rankings.tpl.html',
                    controller: 'AdminRankings',
                    controllerAs: 'adminRankings'
                }
            }
        });
    }

}());
