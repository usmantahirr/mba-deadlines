(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .controller('AdminRankings', AdminRankings);
     AdminRankings.$inject = ['schoolsService', '$rootScope'];
    function AdminRankings(schoolsService, $rootScope) {

        var vm = this;

        vm.searchQuery = '';

        vm.data = [];

        init();

        vm.getRankStatus = getRankStatus;

        function getRankStatus (status) {
            var ret = '';
            if (status === 'up') {
                ret = 'up green';
            } else if (status === 'down') {
                ret = 'down red';
            } else {
                ret = 'minus yellow';
            }
            return ret;
        }

        function init() {
            $rootScope.isLoading = true;
            schoolsService.rest.query(function(schools) {
                debugger;
                vm.data = schools;
                $rootScope.isLoading = false;
            });
        }
    }
}());
