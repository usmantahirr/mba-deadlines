(function () {
    'use strict';

    angular
        .module('mba-deadlines')
        .factory('adminRankingsSvc', adminRankingsSvc);

    adminRankingsSvc.$inject = ['BASE_URL', '$resource'];

    function adminRankingsSvc(BASE_URL, $resource) {
        var rankingsResource = $resource(BASE_URL + '');

        return {
            rest: rankingsResource
        }
    }

}());
