(function() {
    "use strict";

    angular
        .module('mba-deadlines')
        .config(adminStatsRouter);

    adminStatsRouter.$inject = ['$stateProvider'];
    function adminStatsRouter($stateProvider) {
        $stateProvider.state('adminStats', {
            parent: 'admindashboard',
            url: '/stats',
            views: {
                'adminContent': {
                    templateUrl: 'components/admin-dashboard/admin-stats/admin-stats.tpl.html',
                    controller: 'AdminStats',
                    controllerAs: 'adminStats'
                }
            }
        });
    }

}());
