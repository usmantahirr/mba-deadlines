(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .controller('AdminStats', AdminStatsController);
    //AdminStatsController.$inject = ['$scope'];
    function AdminStatsController() {

        // Local Variables
        var vm = this;

        vm.searchQuery = '';
        vm.topSchools = [{
            "id": 1,
            "icon": "images/uni-badges/harvard.png",
            "school": "Harvard University",
            "address": "Cambridge, MA 02138, United States",
            "applications":2390
        }, {
                "id": 2,
                "icon": "images/uni-badges/cambridge.png",
                "school": "Cambridge",
                "address": "Cambridge, England, United Kingdom",
                "applications":1130
            }, {
                "id": 3,
                "icon": "images/uni-badges/stanford.png",
                "school": "Stanford University",
                "address": "450 Serra Mall, Stanford, CA 94305, United States",
                "applications":890
            },
            {
                "id": 4,
                "icon": "images/uni-badges/mit.png",
                "school": "MIT",
                "address": "77 Massachusetts Ave, Cambridge, MA 02139, United States",
                "applications":560
            },
            {
                "id": 5,
                "icon": "images/uni-badges/yale.png",
                "school": "Yale University",
                "address": "New Haven, CT 06520, Andorra",
                "applications":400
            }
        ];


    }
}());
