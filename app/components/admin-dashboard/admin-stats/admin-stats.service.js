(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .factory('adminStatsSvc', adminStatsSvc);

    adminStatsSvc.$inject = ['BASE_URL', '$resource'];

    function adminStatsSvc (BASE_URL, $resource) {
    	var rankingsResource = $resource(BASE_URL + '');

    	return {
    		rest: rankingsResource
    	}
    }

}());