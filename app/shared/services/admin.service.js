(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .factory('adminService', adminService);

    adminService.$inject = ['BASE_URL', '$resource', '$cookies', '$state', '$rootScope'];
    function adminService (BASE_URL, $resource, $cookies, $state, $rootScope) {

        var adminResource = $resource(BASE_URL + '/admin');

        function adminLogout () {
            $cookies.remove('admin');
            $rootScope.admin = false;
            $state.go('landing-page');
        }

        return {
            rest: adminResource,
            logout: adminLogout
        };
    }

}());
