(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .factory('userService', userService);

    userService.$inject = ['BASE_URL', '$resource'];
    function userService (BASE_URL, $resource) {
        var userResource = $resource(BASE_URL + '/auth', {}, {
            post: {
                'method': 'POST'
            },
            update: {
                'method': 'PUT'
            }
        });
        var userData = $resource(BASE_URL + '/users/:id', {}, {
            update: {
                method: 'PUT'
            }
        });

        return {
            rest: userResource,
            data: userData
        }
    }

}());
