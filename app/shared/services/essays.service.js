(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .factory('essayService', essayService);

    essayService.$inject = ['BASE_URL', '$resource'];
    function essayService (BASE_URL, $resource) {

        var schoolResource = $resource(BASE_URL + '/essays/:user_id/:school_id/:essay_id', {}, {
            update: {
                method: 'PUT'
            }
        });

        return {
            rest: schoolResource
        };
    }

}());
