(function() {
    'use strict';

    angular
        .module('mba-deadlines')
        .factory('schoolsService', schoolsService);

    schoolsService.$inject = ['BASE_URL', '$resource'];
    function schoolsService (BASE_URL, $resource) {

        var schoolResource = $resource(BASE_URL + '/schools/:school_id');
        var bookmarkedResource = $resource(BASE_URL + '/bookmarks/:user_id', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            delete: {
                method: 'POST'
            }
        });


        return {
            rest: schoolResource,
            bookmarks: bookmarkedResource
        };
    }

}());
