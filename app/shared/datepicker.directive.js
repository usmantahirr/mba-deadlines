(function () {
    angular
        .module('mba-deadlines')
        .directive('datepicker', function () {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function (scope, element, attrs, ngModelCtrl) {
                    $('#'+attrs.id).datetimepicker({
                        dateFormat: "M dd, yy",
                        timeFormat: 'hh:mmTT Z',
                        controlType: 'select',
                        onSelect: function (date) {
                            ngModelCtrl.$setViewValue(date);
                            scope.$apply();
                        }
                    });
                }
            }
        })
}());
