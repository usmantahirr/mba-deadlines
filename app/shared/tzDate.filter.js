(function () {
    'use strict';

    angular
        .module('mba-deadlines')
        .filter('tzDate', timeZoneFilter);

    timeZoneFilter.$inject = [];

    function timeZoneFilter($scope) {
        return function(input, tz) {
            var date = moment(input, "MMM DD YYYY hh:mmA Z");
            if (date.toString() === "Invalid date") {
                return "N/A";
            }
            return date.tz(tz).format('MMM DD, YYYY hh:mmA');
        }
    }

}());