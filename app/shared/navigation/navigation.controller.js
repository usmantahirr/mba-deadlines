(function () {
    'use strict';

    angular
        .module('mba-deadlines')
        .controller('Navigation', NavigationController);

    NavigationController.$inject = ['$state', '$cookies', '$rootScope'];
    function NavigationController($state, $cookies, $rootScope) {

        // Local Variables
        var vm = this;

        // Exposed Variables
        vm.state = $state;

        // Exposed Functions
        vm.isLandingPage = isLandingPage;
        vm.isLoginPage = isLoginPage;
        vm.isSignupPage = isSignupPage;
        vm.signout = signout;

        // Implementations
        function isLandingPage() {
            return vm.state.is('landing-page');
        }

        function isLoginPage() {
            return vm.state.is('login');
        }

        function isSignupPage() {
            return vm.state.is('signup');
        }

        function signout() {
            $cookies.remove('auth');
            $rootScope.authorized = false;
            $rootScope.portalUser = {};
            $state.go('landing-page');
        }
    }
}());
