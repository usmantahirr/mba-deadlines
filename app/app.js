(function () {
    'use strict';

    angular
        .module('mba-deadlines', ['ngResource', 'ui.router', 'ngSanitize', 'ngCookies', 'ngQuill', 'flow', 'ngCsvImport'])
        .run(runBlock)
        .config(indexConfig);

    // Run Function
    runBlock.$inject = ['$rootScope', '$state', '$cookies'];

    function runBlock($rootScope, $state, $cookies) {
        $rootScope.authorized = false;
        $rootScope.admin = false;
        $rootScope.hideNavbar = false;
        $rootScope.tz = '';
        $rootScope.state = $state;

        $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams) {
            if (toState.redirectTo) {
                event.preventDefault();
                $state.go(toState.redirectTo);
            }

            // get user login
            if (!$cookies.get('auth')) {
                $rootScope.authorized = false;
            } else {
                $rootScope.authorized = true;
            }

            // get admin login
            if (!$cookies.get('admin')) {
                $rootScope.admin = false;
            } else {
                $rootScope.admin = true;
            }
        });
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toStateParams) {
            if (toState.data) {

                // hide nav bar from admin login
                if (toState.data.noNavbar) {
                    $rootScope.hideNavbar = true;
                } else {
                    $rootScope.hideNavbar = false;
                }

                // Check for user login
                if (toState.data.requireAuth) {
                    if (!$rootScope.authorized) {
                        console.log('not auth');
                        $state.go('login');
                    }
                }
                // check for admin login
                if (toState.data.requireAdminAuth) {
                    if (!$rootScope.admin) {
                        $state.go('landing-page');
                    }
                }
            }
        });

        $('#timezonePicker').timezones().dropdown({
            onChange: function (value) {
                $rootScope.tz = value;
                $rootScope.$apply();
            }
        });
    }

    // Index State (route)
    indexConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];

    function indexConfig($stateProvider, $urlRouterProvider, $httpProvider) {
        $urlRouterProvider.otherwise('/home');

        $stateProvider.state('index', {
            abstract: true,
            views: {
                'navigation': {
                    templateUrl: 'shared/navigation/navigation.tpl.html',
                    controller: 'Navigation',
                    controllerAs: 'nav'
                }
            }
        });

        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
}());
